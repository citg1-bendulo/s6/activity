/*1.
a. The busy executive's database guide & you can combat computer stress
b. cooking with computers
c. Bennet, Abraham & Green,Marjorie
d. Algodata infosystems
e.busy executive's database guide, cooking with computers, straight talk about computers,
but is it user friendly, secrets of silicon valley, & net etiquitte
*/
create DATABASE blog_db;

use blog_db;

-- create users table

CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100),
password VARCHAR(300),
datetime_created DATETIME,
PRIMARY KEY(id)
);
-- create posts table
CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
author_id INT NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_posts_users_id
	FOREIGN KEY(author_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT  
);
-- create post_likes table
use blog_db;
use blog_db;
CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_likes_users_id
	FOREIGN KEY(user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT fk_post_likes_posts_id
    FOREIGN KEY(post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
    
);
-- create post_comments
CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_commented DATETIME NOT NULL,
content VARCHAR(5000),
PRIMARY KEY(id),
CONSTRAINT fk_post_comments_users_id
	FOREIGN KEY(user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT fk_post_comments_posts_id
    FOREIGN KEY(post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
    
);
